// Package splines calculates cubic spline for drawing lines through points.
//
// The spline calculations are based on a
// [Mosi's Math](https://www.mosismath.com/AngleSplines/EndSlopeSplines.html) post.
package splines

import (
	"fmt"

	"gonum.org/v1/gonum/mat"
)

// SolveSpline returns a slice with 'n-1' [Polynomial]s for n values of 'y'.
func SolveSpline(ys []float64) ([]Polynomial, error) {
	return SolveSplineWithConstraint(ys, 0, 0)
}

// SolveSplineWithConstraint returns a slice with n-1 Polynomials for n values of y.
// start and end values determine the velocity of the start and end point.
func SolveSplineWithConstraint(ys []float64, start, end float64) ([]Polynomial, error) {
	solution, err := calculateParameters(ys, start, end)
	if err != nil {
		return nil, err
	}

	return polynomialsFromSolution(ys, solution), nil
}

func calculateParameters(ys []float64, start, end float64) ([]float64, error) {
	length := len(ys)
	tridiag := constructTridiag(length)
	vector := constructSolutionVector(ys, start, end)

	dst := new(mat.VecDense)

	if err := tridiag.SolveVecTo(dst, false, vector); err != nil {
		return nil, fmt.Errorf("unable to solve tridiagonal matrix: %w", err)
	}

	return dst.RawVector().Data, nil
}

func constructTridiag(n int) *mat.Tridiag {
	diagonal := constructSliceWithDefault(n, 4)
	side := constructSliceWithDefault(n-1, 1)

	diagonal[0] = 2
	diagonal[n-1] = 2

	return mat.NewTridiag(n, side, diagonal, side)
}

func constructSliceWithDefault(length int, def float64) []float64 {
	slice := make([]float64, length)

	for i := range slice {
		slice[i] = def
	}

	return slice
}

func constructSolutionVector(ys []float64, start, end float64) *mat.VecDense {
	length := len(ys)

	values := make([]float64, length)
	for i := range len(values) - 2 {
		values[i+1] = ys[i+2] - ys[i+1]*2 + ys[i]
	}

	values[0] = ys[1] - ys[0] - start
	values[length-1] = end - (ys[len(ys)-1] - ys[len(ys)-2])

	return mat.NewVecDense(length, values)
}

// polynomialsFromSolution returns a slice with coefficients based on the given y and D values.
func polynomialsFromSolution(ys, ds []float64) []Polynomial {
	coefficients := make([]Polynomial, len(ds)-1)
	for i := range len(ds) - 1 {
		coefficients[i].A = ys[i]
		coefficients[i].B = ys[i+1] - ys[i] - (ds[i+1] + 2*ds[i])
		coefficients[i].C = ds[i] * 3
		coefficients[i].D = ds[i+1] - ds[i]
	}

	return coefficients
}
