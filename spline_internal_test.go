package splines

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
	"gonum.org/v1/gonum/lapack/lapack64"
)

func Test_constructTridiag(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		n    int
		want lapack64.Tridiagonal
	}{
		"matrix of size 2": {
			n: 2,
			want: lapack64.Tridiagonal{
				N:  2,
				DL: []float64{1},
				D:  []float64{2, 2},
				DU: []float64{1},
			},
		},
		"matrix of size 4": {
			n: 4,
			want: lapack64.Tridiagonal{
				N:  4,
				DL: []float64{1, 1, 1},
				D:  []float64{2, 4, 4, 2},
				DU: []float64{1, 1, 1},
			},
		},
		"matrix of size 6": {
			n: 6,
			want: lapack64.Tridiagonal{
				N:  6,
				DL: []float64{1, 1, 1, 1, 1},
				D:  []float64{2, 4, 4, 4, 4, 2},
				DU: []float64{1, 1, 1, 1, 1},
			},
		},
		"matrix of size 1": {
			n: 1,
			want: lapack64.Tridiagonal{
				N:  1,
				DL: []float64{},
				D:  []float64{2},
				DU: []float64{},
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			if got := constructTridiag(tt.n).RawTridiagonal(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("constructTridiag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_constructSolutionVector(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		ys   []float64
		want []float64
	}{
		"0 to 4": {
			ys:   []float64{0, 1, 2, 3, 4},
			want: []float64{1, 0, 0, 0, -1},
		},
		"All zero": {
			ys:   make([]float64, 5),
			want: make([]float64, 5),
		},
		"4 to 1": {
			ys:   []float64{4, 3, 2, 1},
			want: []float64{-1, 0, 0, 1},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got := constructSolutionVector(tt.ys, 0, 0).RawVector().Data
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_solveTridiag(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		ys, want []float64
		wantErr  require.ErrorAssertionFunc
	}{
		"1 to 4": {
			ys:      []float64{1, 2, 3, 4},
			want:    []float64{0.6, -0.2, 0.2, -0.6},
			wantErr: require.NoError,
		},
		"1 to 1": {
			ys:      []float64{1, 1, 1, 1},
			want:    []float64{0, 0, 0, 0},
			wantErr: require.NoError,
		},
		"random values": {
			ys:      []float64{3, 2, 8, 9},
			want:    []float64{-5.6 / 3, 8.2 / 3, -6.2 / 3, 1.6 / 3},
			wantErr: require.NoError,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ys := tt.ys
			solutionVector, err := calculateParameters(ys, 0, 0)
			tt.wantErr(t, err)

			require.InDeltaSlice(t, tt.want, solutionVector, 1e-12)

			checkSolution(t, ys, solutionVector)
		})
	}
}

func checkSolution(t *testing.T, ys, solutionVector []float64) {
	t.Helper()

	n := len(solutionVector)

	expected := ys[1] - ys[0]
	actual := 2*solutionVector[0] + solutionVector[1]
	require.InDelta(t, expected, actual, 1e-12)

	for i := 1; i < n-1; i++ {
		expected = ys[i+1] - 2*ys[i] + ys[i-1]
		actual = solutionVector[i-1] + 4*solutionVector[i] + solutionVector[i+1]
		require.InDelta(t, expected, actual, 1e-12)
	}

	expected = -ys[n-1] + ys[n-2]
	actual = solutionVector[n-2] + 2*solutionVector[n-1]
	require.InDelta(t, expected, actual, 1e-12)
}

func Fuzz_solveTridiag(f *testing.F) {
	f.Add([]byte{1, 2, 3, 4})

	f.Fuzz(func(t *testing.T, y []byte) {
		if len(y) < 2 {
			t.Skip()
		}

		ys := make([]float64, len(y))
		for i := range y {
			ys[i] = float64(int(y[i]) - 1<<7)
		}

		solutionVector, err := calculateParameters(ys, 0, 0)
		require.NoError(t, err)

		checkSolution(t, ys, solutionVector)
	})
}
