package splines

import (
	"math"
)

// Polynomial describes a fourth order polynomial curve.
// The polynomial has the form of "a + b*t + c*t^2 + d*t^3".
type Polynomial struct {
	A, B, C, D float64
}

// Calculate the polynomial for a given value t.
func (c Polynomial) Calculate(t float64) float64 {
	return c.A + c.B*t + c.C*math.Pow(t, 2) + c.D*math.Pow(t, 3)
}
