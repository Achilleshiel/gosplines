package splines_test

import (
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/require"
	splines "gitlab.com/Achilleshiel/gosplines"
)

func TestSolveSpline(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		points []float64
	}{
		"spline 1": {
			points: []float64{0, 1, 2},
		},
		"spline 2": {
			points: []float64{0, 1, 2, -1, 3, 1.5},
		},
		"spline 3": {
			points: []float64{2.4, 5, 2.6, -12.34, 0, 1.65},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			spline, err := splines.SolveSpline(tt.points)
			require.NoError(t, err)
			testSpline(t, spline)
		})
	}
}

func TestSolveSplineWithConstraint(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		points     []float64
		start, end float64
	}{
		"spline 1": {
			points: []float64{0, 1, 2},
			start:  0.1,
			end:    10000.0,
		},
		"spline 2": {
			points: []float64{0, 1, 2, -1, 3, 1.5},
			start:  -240,
			end:    10,
		},
		"spline 3": {
			points: []float64{2.4, 5, 2.6, -12.34, 0, 1.65},
			start:  25000,
			end:    0,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			spline, err := splines.SolveSplineWithConstraint(tt.points, tt.start, tt.end)
			require.NoError(t, err)
			testSpline(t, spline)
		})
	}
}

func testSpline(t *testing.T, spline []splines.Polynomial) {
	t.Helper()

	for i := range len(spline) - 1 {
		testSplineContinuity(t, spline[i], spline[i+1])
		testSplineFirstOrderDerivativeContinuity(t, spline[i], spline[i+1])
		testSplineSecondOrderDerivativeContinuity(t, spline[i], spline[i+1])
	}
}

func testSplineContinuity(t *testing.T, current, next splines.Polynomial) {
	t.Helper()

	currentForTIsOne := current.A + current.B + current.C + current.D
	nextForTIsZero := next.A
	compareValues(t, currentForTIsOne, nextForTIsZero)
}

func compareValues(t *testing.T, currentForTIsOne float64, nextForTIsZero float64) {
	t.Helper()

	if currentForTIsOne < 1 && currentForTIsOne > -1 {
		require.InDelta(t, currentForTIsOne, nextForTIsZero, 1e-12)

		return
	}

	require.InEpsilonf(t, currentForTIsOne, nextForTIsZero, 1e-12, "Current %f, next %f", currentForTIsOne, nextForTIsZero)
}

func testSplineFirstOrderDerivativeContinuity(t *testing.T, this, next splines.Polynomial) {
	t.Helper()

	currentForTIsOne := this.B + 2*this.C + 3*this.D
	nextForTIsZero := next.B
	compareValues(t, currentForTIsOne, nextForTIsZero)
}

func testSplineSecondOrderDerivativeContinuity(t *testing.T, this, next splines.Polynomial) {
	t.Helper()

	currentForTIsOne := 2*this.C + 6*this.D
	nextForTIsZero := 2 * next.C
	compareValues(t, currentForTIsOne, nextForTIsZero)
}

func ExampleSolveSpline() {
	xs := []float64{0, 2, 4, 6, 8}
	ys := []float64{0, 1, 0, -1, 0}
	xPolynomial, _ := splines.SolveSpline(xs)
	yPolynomial, _ := splines.SolveSpline(ys)

	x, y := xPolynomial[0].Calculate(0.6), yPolynomial[0].Calculate(0.6)
	fmt.Printf("first polynomial with t=0.6: x=%0.2f, y=%0.2f\n", x, y)

	x, y = xPolynomial[2].Calculate(0.1), yPolynomial[2].Calculate(0.1)
	fmt.Printf("third polynomial with t=0.1: x=%0.2f, y=%0.2f\n", x, y)

	// output:
	// first polynomial with t=0.6: x=0.93, y=0.59
	// third polynomial with t=0.1: x=4.17, y=-0.17
}

func ExampleSolveSplineWithConstraint() {
	xs := []float64{0, 1}
	ys := []float64{0, 1}
	xPolynomial, _ := splines.SolveSpline(xs)
	yPolynomial, _ := splines.SolveSpline(ys)

	x, y := xPolynomial[0].Calculate(0.5), yPolynomial[0].Calculate(0.5)
	fmt.Printf("polynomial without constraints for t=0.5: x=%0.2f, y=%0.2f\n", x, y)

	xPolynomial, _ = splines.SolveSplineWithConstraint(xs, math.Cos(math.Pi/2), math.Cos(math.Pi))
	yPolynomial, _ = splines.SolveSplineWithConstraint(ys, math.Sin(math.Pi/2), math.Sin(math.Pi))

	x, y = xPolynomial[0].Calculate(0.5), yPolynomial[0].Calculate(0.5)
	fmt.Printf("polynomial with constraints for t=0.5: x=%0.2f, y=%0.2f\n", x, y)

	// output:
	// polynomial without constraints for t=0.5: x=0.50, y=0.50
	// polynomial with constraints for t=0.5: x=0.62, y=0.62
}

func TestSolveSplineWithConstraint2(t *testing.T) {
	t.Parallel()

	alpha := 45.0
	beta := -90.0

	ys := []float64{3, 5, 2, 0.5, 3, 5.5, 1}
	yPolynomials, err := splines.SolveSplineWithConstraint(ys, 5*math.Sin(alpha*math.Pi/180), 5*math.Sin(beta*math.Pi/180))
	require.NoError(t, err)

	yExpected := []splines.Polynomial{{
		A: 3,
		B: 3.5355339059327373,
		C: -0.26859782937836929,
		D: -1.2669360765543678,
	}, {
		A: 5,
		B: -0.80246998248710533,
		C: -4.0694060590414729,
		D: 1.8718760415285782,
	}, {
		A: 2,
		B: -3.3256539759843164,
		C: 1.5462220655442622,
		D: 00.27943191044005405,
	}, {
		A: 0.5,
		B: 0.60508588642437067,
		C: 2.3845177968644244,
		D: -0.489603683288795,
	}, {
		A: 3,
		B: 3.9053104302868342,
		C: 0.91570674699803933,
		D: -2.3210171772848738,
	}, {
		A: 5.5,
		B: -1.2263276075717093,
		C: -6.0473447848565822,
		D: 2.7736723924282911,
	}}

	for i := range yExpected {
		comparePolynomials(t, yExpected[i], yPolynomials[i], 1e-14)
	}

	xs := []float64{0.5, 3.5, 6, 4, 3, 6.5, 7.5}
	xPolynomials, err := splines.SolveSplineWithConstraint(xs, 5*math.Cos(alpha*math.Pi/180), 5*math.Cos(beta*math.Pi/180))
	require.NoError(t, err)

	xExpected := []splines.Polynomial{{
		A: 0.5,
		B: 3.5355339059327378,
		C: -1.2429568037373446,
		D: 0.70742289780460688,
	}, {
		A: 3.5,
		B: 3.1718889918718691,
		C: 0.87931188967647578,
		D: -1.5512008815483447,
	}, {
		A: 6,
		B: 0.2769101265797862,
		C: -3.7742907549685585,
		D: 1.4973806283887721,
	}, {
		A: 4,
		B: -2.7795294981910139,
		C: 0.71785113019775793,
		D: 1.0616783679932562,
	}, {
		A: 3,
		B: 1.8412078661842706,
		C: 3.9028862341775268,
		D: -2.2440941003617971,
	}, {
		A: 6.5,
		B: 2.9146980334539321,
		C: -2.829396066907865,
		D: 0.91469803345393264,
	}}
	for i := range xExpected {
		comparePolynomials(t, xExpected[i], xPolynomials[i], 1e-14)
	}
}
