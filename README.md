# Go Splines

A library that allows you to generate cubic splines.

## Get Go Splines

`go get gitlab.com/Achilleshiel/gosplines`