package splines_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	splines "gitlab.com/Achilleshiel/gosplines"
)

func TestCoefficient_Calculate(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		p    splines.Polynomial
		t    []float64
		want []float64
	}{
		"first order polynomial": {
			p:    splines.Polynomial{A: 1, B: 1, C: 0, D: 0},
			t:    []float64{-1, 0, 0.5, 1, 2},
			want: []float64{0, 1, 1.5, 2, 3},
		},
		"second order polynomial": {
			p:    splines.Polynomial{A: 1, B: 1, C: 1, D: 0},
			t:    []float64{0, 0.5, 1, 2},
			want: []float64{1, 1.75, 3, 7},
		},
		"third order polynomial": {
			p:    splines.Polynomial{A: 1, B: 1, C: 1, D: 1},
			t:    []float64{-1, 0, 0.5, 1, 2},
			want: []float64{0, 1, 1.875, 4, 15},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			for i, timestamp := range tt.t {
				require.InDelta(t, tt.want[i], tt.p.Calculate(timestamp), 1e-12)
			}
		})
	}
}

func comparePolynomials(t *testing.T, expected, actual splines.Polynomial, delta float64) {
	t.Helper()

	require.InDelta(t, expected.A, actual.A, delta)
	require.InDelta(t, expected.B, actual.B, delta)
	require.InDelta(t, expected.C, actual.C, delta)
	require.InDelta(t, expected.D, actual.D, delta)
}
